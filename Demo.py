import numpy as np 
import pandas as pd 

DF = pd.read_csv('styles.csv') 
M  = DF['likes'].quantile(0.5) 

# Look for similar items with min num of Likes. 
def demo(gender="Unisex",master="Apparel",subcat="",article="",year=""): 
    items = DF.copy().loc[DF['likes'] >= M] 
    items = items.loc[items['gender'] == gender] 
    items = items.loc[items['masterCategory'] == master] 
    if subcat  != "": items = items.loc[items['subCategory'] == subcat] 
    if article != "": items = items.loc[items['articleType'] == article] 
    if year    != "": items = items.loc[items['year'] >= year] 
    items = items.sort_values('likes', ascending=False) 
    print(items.head(10)); print("\n"); 


demo()                                              # Suggest anything based on pre-set choices. 
demo("Men","Footwear")                              # Suggest men's shoes. 
demo("Girls","Apparel","Topwear","Tops")            # Specific suggestions from any year. 
demo("Women","Apparel","Topwear","Tshirts","2013")  # Specific suggestions on recent fashion. 

