import numpy as np 
import pandas as pd 
from ast import literal_eval 
from sklearn.metrics.pairwise import linear_kernel 
from sklearn.metrics.pairwise import cosine_similarity 
from sklearn.feature_extraction.text import TfidfVectorizer 
from sklearn.feature_extraction.text import CountVectorizer 

DF = pd.read_csv('styles.csv') 
M  = DF['likes'].quantile(0.5) 
CP = DF.copy() 


def get_list(x): 
    if isinstance(x, list): 
        names = [i['name'] for i in x] 
        if len(names) > 3: names = names[:3] 
        return names 
    return [] 

def clean_data(x): 
    if isinstance(x, list): 
        return [str.lower(i.replace(" ", "")) for i in x] 
    else: 
        if isinstance(x, str): return str.lower(x.replace(" ", "")) 
        else: return '' 

def create_soup(x): 
    return ' '.join(x['articleType'])+' '+' '.join(x['season'])+' '+x['usage']+' '+' '.join(x['productDisplayName']) 


features = ['articleType', 'season', 'usage', 'productDisplayName'] 
for feature in features:
    DF[feature] = DF[feature].apply(clean_data)
DF['soup'] = DF.apply(create_soup, axis=1) 

count = CountVectorizer(stop_words='english') 
count_matrix = count.fit_transform(DF['soup']) 
cosine_sim = cosine_similarity(count_matrix, count_matrix) 
indices = pd.Series(DF.index, index=DF['articleType'])


# Look for similar items with similar keywords in product description. 
def suggest(description="", cosine_sim=cosine_sim): 
    idx = indices[description] 
    sim_scores = list(enumerate(cosine_sim[idx])) 
    sim_scores = sorted(sim_scores, key=lambda x: x[1].any(), reverse=True) 
    sim_scores = sim_scores[1:11] 
    items = [i[0] for i in sim_scores] 
    print(CP.iloc[items].sort_values('likes', ascending=False)); print("\n"); 


suggest("trackpants") 
suggest("handbags") 
suggest("tshirts") 
suggest("watches") 



